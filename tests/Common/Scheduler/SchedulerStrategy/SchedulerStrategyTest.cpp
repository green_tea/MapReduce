#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>

#include "../../../../src/Common/Scheduler/SchedulerStrategy/ISchedulerStrategy.hpp"
#include "../../../../src/Common/Scheduler/SchedulerStrategy/RoundRobin.hpp"

#include <vector>

TEST_CASE("Round robin scheduler strategy", "[RoundRobin]")
{
    ISchedulerStrategy* round_robin = new RoundRobin;

    SECTION("Try to add one simple task") {
        Task* task = new Task("A");

        REQUIRE(0 == round_robin->taskSize());

        round_robin->addTask(task);

        REQUIRE(1 == round_robin->taskSize());
        REQUIRE(!task->completed);

        Pod* pod = new Pod;

        REQUIRE(0 == pod->task_completed);
        REQUIRE(0 == round_robin->podSize());

        round_robin->addPod(pod);

        REQUIRE(0 == round_robin->taskSize());
        REQUIRE(task->completed);
        REQUIRE(1 == pod->task_completed);
        REQUIRE(1 == round_robin->podSize());

        delete task;

        round_robin->deletePod(pod);

        REQUIRE(0 == round_robin->podSize());

        round_robin->deletePod(pod);

        REQUIRE(0 == round_robin->podSize());

        delete pod;
    }

    SECTION("Try to add many tasks") {
        Task* task1 = new Task("1");
        Task* task2 = new Task("2");
        Task* task3 = new Task("3");
        Task* task4 = new Task("4");
        Task* task5 = new Task("5");
        Task* task6 = new Task("6");

        Pod* pod1 = new Pod;
        Pod* pod2 = new Pod;

        pod1->can_work = false;
        pod1->pod_name = "A";
        pod2->can_work = false;
        pod2->pod_name = "B";

        round_robin->addTask(task1);
        round_robin->addTask(task2);
        round_robin->addTask(task3);
        round_robin->addTask(task4);
        round_robin->addTask(task5);
        round_robin->addTask(task6);

        REQUIRE(0 == round_robin->podSize());
        REQUIRE(6 == round_robin->taskSize());

        round_robin->addPod(pod1);

        REQUIRE(1 == round_robin->podSize());
        REQUIRE(6 == round_robin->taskSize());

        round_robin->addPod(pod2);

        REQUIRE(2 == round_robin->podSize());
        REQUIRE(6 == round_robin->taskSize());

        round_robin->startAllPods();

        REQUIRE(0 == round_robin->taskSize());
        REQUIRE(2 == round_robin->podSize());

        REQUIRE(task1->completed);
        REQUIRE(task2->completed);
        REQUIRE(task3->completed);
        REQUIRE(task4->completed);
        REQUIRE(task5->completed);
        REQUIRE(task6->completed);

        REQUIRE(3 == pod1->task_completed);
        REQUIRE(3 == pod2->task_completed);

        delete task1;
        delete task2;
        delete task3;
        delete task4;
        delete task5;
        delete task6;

        delete pod1;
        delete pod2;
    }
}