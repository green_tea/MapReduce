#ifndef MAPREDUCE_MASTERNODE_H
#define MAPREDUCE_MASTERNODE_H

#include <string>
#include <vector>

#include "../Common/Pipes/WriterPipe.hpp"
#include "../Common/Pipes/ReaderPipe.hpp"
#include "../Common/types.h"

class MasterNode {
public:
    WriterPipe getFilesystemDescriptor(const FileAddress& address);

    std::vector<FileAddress> mapReduce(
            const FileAddress & map_file,
            const FileAddress & reduce_file,
            std::vector<FileAddress> data
    );

    ReaderPipe pipedMapReduce(const FileAddress & map_file, const FileAddress & reduce_file, const std::vector<WriterPipe>& data);
    void stopPipedMapReduce();
};


#endif //MAPREDUCE_MASTERNODE_H
