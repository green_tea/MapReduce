#ifndef MAPREDUCE_NAMESERVER_H
#define MAPREDUCE_NAMESERVER_H

#include "../Common/Pipes/ReaderPipe.hpp"
#include "../Common/Pipes/WriterPipe.hpp"

#include "../Common/types.h"

#include "Node.hpp"

#include <vector>
#include <unordered_map>

class NameServer {
private:
    std::vector<Node*> nodes_;
    std::unordered_map<FileAddress, std::vector<Node*>> file_address_to_nodes_;

public:
    ReaderPipe read(const FileAddress& addr);

    WriterPipe write(const FileAddress& addr);

    void addNode(const Node& node);

    void deleteNode(const Node& node);
};

#endif //MAPREDUCE_NAMESERVER_H
