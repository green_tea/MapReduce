#ifndef MAPREDUCE_NODE_H
#define MAPREDUCE_NODE_H

#include <vector>

#include "../Common/types.h"

#include "../Common/Pipes/WriterPipe.hpp"
#include "../Common/Pipes/ReaderPipe.hpp"

class Node {
public:
    Node();

    std::vector<FileAddress> files;

    ReaderPipe read(const FileAddress& address);

    WriterPipe write(const FileAddress& address);

    void deleteFile();
};

#endif //MAPREDUCE_NODE_H
