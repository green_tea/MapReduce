#ifndef MAPREDUCE_ROUNDROBIN_H
#define MAPREDUCE_ROUNDROBIN_H

#include "ISchedulerStrategy.hpp"

#include "../Pod.hpp"
#include "../Task.hpp"

#include <unordered_map>

class RoundRobin : public ISchedulerStrategy {
private:
    bool tryToStartTask() {
        if (tasks.empty() || empty_pods.empty()) {
            return false;
        }

        Pod* empty_pod = empty_pods.front();
        empty_pods.pop();

        Task* task = tasks.front();
        tasks.pop();

        empty_pod->processTask(task, [this](Pod* pod, Task* task) { this->taskCompleted(pod, task); });

        return true;
    }

public:

};


#endif //MAPREDUCE_ROUNDROBIN_H
