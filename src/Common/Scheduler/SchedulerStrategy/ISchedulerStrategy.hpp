#ifndef MAPREDUCE_SCHEDULERSTRATEGY_H
#define MAPREDUCE_SCHEDULERSTRATEGY_H

#include "../Pod.hpp"

#include <unordered_map>
#include <queue>

class ISchedulerStrategy {
protected:
    std::unordered_map<std::string, Pod*> pods;
    std::queue<Task*> tasks;
    std::queue<Pod*> empty_pods;

    virtual bool tryToStartTask() = 0;

public:
    void addTask(Task* task) {
        tasks.push(task);
        tryToStartTask();
    };

    void addPod(Pod* pod) {
        if (pods.find(pod->pod_name) != pods.end()) {
            return;
        }

        pods[pod->pod_name] = pod;

        if (!pod->can_work) {
            return;
        }

        empty_pods.push(pod);
        tryToStartTask();
    };

    void startAllPods() {
        for (auto it = pods.begin(); it != pods.end(); ++it) {
            if (!it->second->can_work) {
                it->second->can_work = true;
                empty_pods.push(it->second);
            }
        }

        while (tryToStartTask());
    }

    void deletePod(Pod* pod) {
        if (pods.find(pod->pod_name) == pods.end()) {
            return;
        }

        if (pods[pod->pod_name]->current_task) {
            addTask(pods[pod->pod_name]->current_task);
        }

        pods.erase(pod->pod_name);

        std::queue<Pod*> new_pods;
        while (!empty_pods.empty()) {
            Pod* cur_pod = empty_pods.front();
            empty_pods.pop();

            if (cur_pod->pod_name == pod->pod_name) {
                continue;
            }

            new_pods.push(cur_pod);
        }

        empty_pods = std::move(new_pods);
    };

    void taskCompleted(Pod* pod, Task* task) {
        empty_pods.push(pod);
        tryToStartTask();
    }

    std::size_t taskSize() {
        return this->tasks.size();
    }

    std::size_t podSize() {
        return this->pods.size();
    }
};


#endif //MAPREDUCE_SCHEDULERSTRATEGY_H
