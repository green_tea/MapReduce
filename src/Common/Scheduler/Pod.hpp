#ifndef MAPREDUCE_POD_HPP
#define MAPREDUCE_POD_HPP

#include "../types.h"
#include "Task.hpp"

#include <string>
#include <functional>

class Pod {
public:
    std::string pod_name;
    Task* current_task;
    std::size_t task_completed = 0;
    bool can_work = true;

    void processTask(Task* task, std::function<void(Pod* pod, Task* task)> func) {
        if (!can_work) {
            return;
        }

        task->process();
        ++task_completed;

        func(this, task);
    };
};


#endif //MAPREDUCE_POD_HPP
