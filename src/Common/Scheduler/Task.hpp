#ifndef MAPREDUCE_TASK_HPP
#define MAPREDUCE_TASK_HPP

#include <string>
#include <iostream>

class Task {
public:
    std::string task_id;
    bool completed = false;

    explicit Task(const std::string& task_id) {
        this->task_id = task_id;
    }

    void process() {
        std::cout << "this task: " << task_id << "\n";
        completed = true;
    };
};


#endif //MAPREDUCE_TASK_HPP
