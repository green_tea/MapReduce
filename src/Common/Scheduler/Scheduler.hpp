#ifndef MAPREDUCE_SCHEDULER_HPP
#define MAPREDUCE_SCHEDULER_HPP

#include "SchedulerStrategy/ISchedulerStrategy.hpp"

#include <unordered_map>
#include <string>

class Scheduler {
    ISchedulerStrategy strategy;

public:
    void addPod(const Pod& pod) {
        strategy.addPod(pod);
    };

    void removePod(const Pod& pod) {
        strategy.removePod(pod);
    };

    void clearTasks() {
        strategy.clearTasks();
    };

    void addTask(const Task& task) {
        strategy.addTask(task);
    };
};


#endif //MAPREDUCE_SCHEDULER_HPP
