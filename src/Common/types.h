#ifndef MAPREDUCE_TYPES_H
#define MAPREDUCE_TYPES_H

#include <string>

using FileAddress = std::string;

class ISchedulerStrategy;

class Pod;

class Task;

#endif //MAPREDUCE_TYPES_H
