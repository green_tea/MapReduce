#ifndef MAPREDUCE_PIPE_H
#define MAPREDUCE_PIPE_H

#include "ReaderPipe.hpp"
#include "WriterPipe.hpp"

class Pipe {
public:
    WriterPipe writer_pipe;
    ReaderPipe reader_pipe;
};


#endif //MAPREDUCE_PIPE_H
